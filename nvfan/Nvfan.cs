﻿using System;
using System.Diagnostics;
using System.IO;
using System.Text.Json;
using System.Threading.Tasks;

public class CSerializableNvFanControl
{
    #region Constantes y variables

    const string DIR_CONFIG = "/etc/nvfan/";

    #endregion

    #region Accesos

    public int T0 { get; set; }
    public int F0 { get; set; }

    public int T1 { get; set; }
    public int F1 { get; set; }

    public int T2 { get; set; }
    public int F2 { get; set; }

    public int T3 { get; set; }
    public int F3 { get; set; }

    public int T4 { get; set; }
    public int F4 { get; set; }

    public float Delay { get; set; }

    #endregion

    #region Constructores

    public CSerializableNvFanControl() 
    {
        HabilitarModoPersistente();
        HabilitarControlVentilador();
    }

    #endregion

    #region Metodos publicos

    /// <summary>
    /// Guarda la configuración de una instancia de clase a un fichero "NvFan.json"
    /// </summary>
    public static void GuardarConfiguracion(CSerializableNvFanControl configNvFan)
    {
        var configSerializacion = new JsonSerializerOptions() { WriteIndented = true };
        File.WriteAllText($"{DIR_CONFIG}nvfan.json", JsonSerializer.Serialize(configNvFan, configSerializacion));
    }

    public static void ComprobarConfiguracion()
    {
        if (!File.Exists($"{DIR_CONFIG}nvfan.json"))
        {
            Directory.CreateDirectory(DIR_CONFIG);
            GuardarConfiguracion(new CSerializableNvFanControl()
            {
                T0 = 30,
                F0 = 15,
                T1 = 35,
                F1 = 35,
                T2 = 50,
                F2 = 60,
                T3 = 55,
                F3 = 80,
                T4 = 60,
                F4 = 100,
                Delay = 1
            });
            Environment.Exit(0);
        }        
    }

    /// <summary>
    /// Carga la configuración principal del programa desde un fichero "NvFan.json"
    /// </summary>
    /// <returns>Instancia de clase configurada</returns>
    public static CSerializableNvFanControl CargarConfiguracion()
    {
        string ficheroConfig = File.ReadAllText($"{DIR_CONFIG}nvfan.json");
        return JsonSerializer.Deserialize<CSerializableNvFanControl>(ficheroConfig);
    }

    public async Task Monitorizacion()
    {
        while (true)
        {
            int temp = ObtenerTemperatura();
            if (temp <= T0)
                ConfigurarVentilador(F0);
            else if (temp <= T1)
                ConfigurarVentilador(F1);
            else if (temp <= T2)
                ConfigurarVentilador(F2);
            else if (temp <= T3)
                ConfigurarVentilador(F3);
            else
                ConfigurarVentilador(F4);
            await Task.Delay(TimeSpan.FromSeconds(Delay));
        }
    }

    #endregion

    #region Metodos privados

    string EnviarComando(string comando)
    {
        var configProceso = new ProcessStartInfo() 
        {
            FileName = "/bin/bash",
            UseShellExecute = false,
            RedirectStandardOutput = true,
            Arguments = $"-c \"{comando}\"",
        };

        using var proceso = new Process() { StartInfo = configProceso };
        proceso.Start();
        return proceso.StandardOutput.ReadToEnd();
    }

    void HabilitarModoPersistente()
    {
        Console.WriteLine(EnviarComando("nvidia-smi -pm 1"));
    }

    void HabilitarControlVentilador()
    {
        EnviarComando("nvidia-settings -a \"GPUFanControlState=1\"");
    }

    int ObtenerTemperatura()
    {
        return Convert.ToInt16(EnviarComando("nvidia-smi --query-gpu=temperature.gpu --format=csv,noheader"));
    }

    bool ConfigurarVentilador(int porcentajeRPM)
    {
        string retornoCmd = EnviarComando($"nvidia-settings -a \"GPUTargetFanSpeed={porcentajeRPM}\"");
        if (retornoCmd.Contains($"assigned value {porcentajeRPM}"))
            return true;
        return false;
    }

    #endregion
}

