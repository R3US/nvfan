﻿using System.Diagnostics;
using System.Threading.Tasks;

namespace nvfan
{
    internal class Program
    {
        static async Task Main(string[] args)
        {
            Process.GetCurrentProcess().PriorityClass = ProcessPriorityClass.Idle;

            CSerializableNvFanControl.ComprobarConfiguracion();
            var nvFanControl = CSerializableNvFanControl.CargarConfiguracion();
            await nvFanControl.Monitorizacion();
        }
    }
}
