# nvfan

"nvfan" es una herramienta dedicada al control de ventiladores en tarjetas gráficas "NVIDIA", para sistemas operativos "Linux" de 64 bits.

## Requisitos

- "Driver" privativo de "NVIDIA".

- net6.0.

## Modo de uso

- El binario se ha compilado en un único fichero. Es un requisito indispensable para un funcionamiento ideal, sin desperdigarse las dependencias.

- Descargar el binario o compilarlo (la descarga es obligatoria, ya que necesitamos el script en "bash" incluído para instalarlo).

- Mover el binario compilado a la carpeta descomprimida del fichero descargado, o si queremos utilizar ya la versión pre-compilada, símplemente ejecutar con "sudo ./install.sh".

- Modificar el rango de trabajo de temperaturas a gusto del usuario, desde el fichero de configuración, ubicado en el directorio "/etc/nvfan/nvfan.json" y reiniciar el servicio para aplicar los cambios, con "sudo systemctl restart nvfan".

## Descargas:

- Binarios:

```
https://www.mediafire.com/file/6ioq19otqgkked6/nvfan.7z/file

```

